#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.apache.camel.component.cxf.CxfComponent;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ${package}.cxf.EnableCORSInterceptor;
import ${package}.cxf.UserCredentialInterceptor;
import ${package}.cxf.WSSecurityInterceptor;
import ${package}.ws.CXFServiceImpl;

@Configuration
public class AppConfiguration implements CamelContextAware {

	@Autowired
	private AppProperties properties;

	private CamelContext context;

	protected WSS4JInInterceptor wss4j() {

		Map<String, Object> map = new HashMap<>();
		map.put("action", "UsernameToken");
		map.put("passwordType", "PasswordText");

		WSS4JInInterceptor wss4j = new WSS4JInInterceptor(map);

		return wss4j;
	}

	protected WSSecurityInterceptor wssin() {

		WSSecurityInterceptor wssin = new WSSecurityInterceptor();
		wssin.setVerificationPassword(properties.getVerificationPassword());
		wssin.setVerificationUsername(properties.getVerificationUserName());

		wssin.setReportFault(true);

		return wssin;
	}

	protected UserCredentialInterceptor basic() {

		UserCredentialInterceptor basic = new UserCredentialInterceptor();
		basic.setVerificationPassword(properties.getVerificationPassword());
		basic.setVerificationUsername(properties.getVerificationUserName());

		basic.setReportFault(true);

		return basic;
	}
	
	@Bean
    public ServletRegistrationBean dispatcherServlet() {
        return new ServletRegistrationBean(new CXFServlet(), properties.getCxfServletContext());
    }

	@Bean
	public CxfEndpoint service() {

		CxfComponent cxfComponent = new CxfComponent(getCamelContext());
		CxfEndpoint serviceEndpoint = new CxfEndpoint(properties.getCxfServiceOptions(), cxfComponent);
		serviceEndpoint.setServiceClass(CXFServiceImpl.class);
		serviceEndpoint.setAddress(properties.getCxfServiceAddres());

		/* Use these 2 interceptors for ws-security */
		serviceEndpoint.getInInterceptors().add(wss4j());
		serviceEndpoint.getInInterceptors().add(wssin());

		/* use this interceptor for http basic authentication */
		serviceEndpoint.getInInterceptors().add(basic());

		serviceEndpoint.getOutInterceptors().add(new EnableCORSInterceptor());

		Map<String, Object> cxfProperties = new HashMap<>();
		cxfProperties.put("dataFormat", "PAYLOAD");
		cxfProperties.put("ws-security.ut.no-callbacks", "true");
		cxfProperties.put("ws-security.validate.token", "false");

		serviceEndpoint.setProperties(cxfProperties);

		return serviceEndpoint;
	}

	public CamelContext getCamelContext() {
		return context;
	}

	public void setCamelContext(CamelContext context) {
		this.context = context;
	}
}
