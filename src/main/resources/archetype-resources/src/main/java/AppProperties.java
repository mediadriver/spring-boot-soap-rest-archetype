#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// CXF parameters
	@Value("${symbol_dollar}{cxf.servlet.context}")
	private String cxfServletContext;

	@Value("${symbol_dollar}{cxf.service.address}")
	private String cxfServiceAddres;
	
	@Value("${symbol_dollar}{cxf.service.options}")
	private String cxfServiceOptions;

	// WS-Security Endpoint parameters
	@Value("${symbol_dollar}{wss.verification.userName}")
	private String verificationUserName;

	@Value("${symbol_dollar}{wss.verification.password}")
	private String verificationPassword;

	// REST Endpoint url
	@Value("${symbol_dollar}{rest.service.url}")
	private String restServiceUrl;

	public String getCxfServletContext() {
		return cxfServletContext;
	}

	public void setCxfServletContext(String cxfServletContext) {
		this.cxfServletContext = cxfServletContext;
	}

	public String getCxfServiceAddres() {
		return cxfServiceAddres;
	}

	public void setCxfServiceAddres(String cxfServiceAddres) {
		this.cxfServiceAddres = cxfServiceAddres;
	}

	public String getCxfServiceOptions() {
		return cxfServiceOptions;
	}

	public void setCxfServiceOptions(String cxfServiceOptions) {
		this.cxfServiceOptions = cxfServiceOptions;
	}

	public String getVerificationUserName() {
		return verificationUserName;
	}

	public void setVerificationUserName(String verificationUserName) {
		this.verificationUserName = verificationUserName;
	}

	public String getVerificationPassword() {
		return verificationPassword;
	}

	public void setVerificationPassword(String verificationPassword) {
		this.verificationPassword = verificationPassword;
	}

	public String getRestServiceUrl() {
		return restServiceUrl;
	}

	public void setRestServiceUrl(String restServiceUrl) {
		this.restServiceUrl = restServiceUrl;
	}

}
