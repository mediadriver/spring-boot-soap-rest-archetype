#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ${package}.AppProperties;

@Component
public class RouteBuilderOne extends RouteBuilder {

	@Autowired
	private AppProperties properties;

	
	@Override
	public void configure() throws Exception {
		
		onException(Exception.class).handled(true).stop();
		
		from("cxf:bean:service").
		log("LOG RECEIVED MESSAGE").
		/* Processor used for any query elements needed for Rest Service call.*/
		process(new Processor() {
		    public void process(Exchange exchange) throws Exception {
		        
		    	String payload = exchange.getIn().getBody(String.class);
		        // do something with the payload and/or exchange here
		        exchange.getIn().setBody("Changed body");
		   
		    }
		})
		// setup headers for the http request to be made
		.setHeader(Exchange.HTTP_METHOD, constant(org.apache.camel.component.http4.HttpMethods.POST))
		.to("http4://"+properties.getRestServiceUrl()).
		log("LOG SENT MESSAGE").
		end();
		
	}

}
